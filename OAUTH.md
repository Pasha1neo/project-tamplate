# OAUTH 2.0

registration -> login -> 2FA -> authorization (oauth) -> authentification (oidc)

# RFC

- rfc6749 Oauth 2.0 - https://datatracker.ietf.org/doc/html/rfc6749
- rfc6750 JWT Bearer - https://datatracker.ietf.org/doc/html/rfc6750
- rfc7591 OAuth 2.0 Dynamic Client Registration Protocol - https://datatracker.ietf.org/doc/html/rfc7591
- rfc7636 PKCE - https://datatracker.ietf.org/doc/html/rfc7636
- rfc7662 OAuth 2.0 Token Introspection - https://datatracker.ietf.org/doc/html/rfc7662
- rfc8414 OAuth 2.0 Authorization Server Metadata - https://datatracker.ietf.org/doc/html/rfc8414
- OAuth 2.0 Security Best Current Practice - https://datatracker.ietf.org/doc/html/draft-ietf-oauth-security-topics

## заметки не забыть прочитать

влияние двух факторной аутентификации (2fa)

oauth 2.1

openid connect

role based access control (RBAC)

Basic authorization - https://datatracker.ietf.org/doc/html/rfc2617

## Ресурсы

- https://accounts.google.com/.well-known/openid-configuration (гугл server metadata)
- https://www.oauth.com/playground/index.html (Игровая площадка)
- https://oauth.net/2/grant-types/
- https://alexbilbie.github.io/guide-to-oauth-2-grants/
- https://aaronparecki.com/oauth-2-simplified/#authorization
- https://drawsql.app/templates/django-oauth-toolkit (Ахуенная диаграмма структуры oauth там есть селект Postgresql)

## Теория

https://authorization-server.com/

## grant types - типы грантов

Список

- Authorization Code Grant +
- Implicit Grant +
- Password Grant +
- Client Credentials +
- Refresh Token +
- Device Code
- PKCE (RFC 7636)

## 4.1. Authorization Code Grant

- https://datatracker.ietf.org/doc/html/rfc6749#section-4.1

### 4.1.1. Authorization Request

Получение кода происходит путём редиректов
1 - при авторизации мы направляемся на сервер авторизации на endpoint Oauth где указываем query search params
2 - сервер авторизации всё это обрабатываем

```sh
https://server.example.com/authorize?response_type=code&client_id=s6BhdRkqt3&state=xyz&redirect_uri=https%3A%2F%2Fclient%2Eexample%2Ecom%2Fcb

{
	"response_type": "code", // code это предварительный этап типа авторизации Authorization Code - нужен для получения кода
	"client_id": "s6BhdRkqt3", // Это клиентский id сгенерированный Сервером авторизации
	"redirect_uri"?: "https%3A%2F%2Fclient%2Eexample%2Ecom%2Fcb",  // куда перенаправить пользователя после авторизации
	"scope"?: "profile user" // права доступа
	"state"?: "xyz" // это данные которые несут в себе информацию прописанную самим клиентом
}
```

### 4.1.2. Authorization Response

3 - сервер авторизации всё проверил и сделал редирект на redirect_uri или на ранее заданный url клиента
3 - мы попали обратно на клиент и у нас в query лежат параметры code и state если указывали

```sh

редирект

https://client.example.com/cb?code=SplxlOBeZQQYbYS6WxSbIA&state=xyz

```

### 4.1.2.1. Error Response

```sh

редирект

https://client.example.com/cb?error=access_denied&state=xyz

описание ошибок - https://datatracker.ietf.org/doc/html/rfc6749#section-4.1.2.1

{
	"error": "invalid_request | unauthorized_client | access_denied | unsupported_response_type | invalid_scope | server_error | temporarily_unavailable",
	"state"?: "",
	"error_description"?: "",
	"error_uri"?: "",
}
```

### 4.1.3. Access Token Request

```sh

METHOD: POST
Authorization: Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW
Content-Type: application/x-www-form-urlencoded

https://server.example.com/grant_type=authorization_code&code=SplxlOBeZQQYbYS6WxSbIA&redirect_uri=https%3A%2F%2Fclient%2Eexample%2Ecom%2Fcb

{
	"grant_type": "authorization_code", // authorization_code это тип авторизации - Authorization Code
	"code": "SplxlOBeZQQYbYS6WxSbIA", // Это клиентский id сгенерированный Сервером авторизации
	"redirect_uri"?: "https%3A%2F%2Fclient%2Eexample%2Ecom%2Fcb",  // куда перенаправить пользователя после авторизации
}
```

### 4.1.4. Access Token Response

```sh
Status: 200 OK // статус
Content-Type: application/json;charset=UTF-8 // ответ JSON
Cache-Control: no-store // без кеширования
Pragma: no-cache

{
  "access_token":"2YotnFZFEjr1zCsicMWpAA", // токен доступа чаще всего это JWT
  "token_type":"example", // тип токена чаще всего это Bearer
  "expires_in"?:3600, // время жизни токена 1 час
  "refresh_token"?:"tGzv3JOkF0XG5Qx2TlKWIA",  // токен обновления чаще всего это JWT
  "example_parameter"?:"example_value" // доп параметры которые сервер присылает в ответе
}

про refresh токен читать в пункте https://datatracker.ietf.org/doc/html/rfc6749#section-5.1

```

### (5.2) Access Token Error Response

## 4.2. Implicit Grant

- https://datatracker.ietf.org/doc/html/rfc6749#section-4.2
- Нет поддержки refresh_token
- Сам поток устарел и рекомендуется использовать PKCE (RFC 7636)

### 4.2.1. Authorization Request

Получение кода происходит путём редиректов
1 - при авторизации мы направляемся на сервер авторизации на endpoint Oauth где указываем query search params
2 - сервер авторизации всё это обрабатываем

```sh

https://server.example.com/authorize?response_type=token&client_id=s6BhdRkqt3&state=xyz&redirect_uri=https%3A%2F%2Fclient%2Eexample%2Ecom%2Fcb

{
	"response_type": "token", // token это предварительный этап типа авторизации Implicit Grant - нужен для получения кода
	"client_id": "s6BhdRkqt3", // Это клиентский id сгенерированный Сервером авторизации
	"redirect_uri"?: "https%3A%2F%2Fclient%2Eexample%2Ecom%2Fcb",  // куда перенаправить пользователя после авторизации
	"scope"?: "profile user" // права доступа
	"state"?: "xyz" // это данные которые несут в себе информацию прописанную самим клиентом
}
```

### 4.2.2. Access Token Response

3 - сервер авторизации всё проверил и сделал редирект на redirect_uri или на ранее заданный url клиента
3 - мы попали обратно на клиент и у нас в query лежат параметры access_token, expires_in, token_type и state если указывали

```sh

Редирект

!!! Заметь тут не ? а фрагмент #

http://example.com/cb#access_token=2YotnFZFEjr1zCsicMWpAA&state=xyz&token_type=example&expires_in=3600

{
"access_token": "2YotnFZFEjr1zCsicMWpAA", // токен доступа
"token_type": "example", // параметры ниже уже были описаны в Authorization Code см выше.
"expires_in"?: "3600", //
"state"?: "xyz", //
}
```

### 4.2.2.1. Error Response

https://datatracker.ietf.org/doc/html/rfc6749#section-4.2.2.1

```sh

редирект

!!! Заметь тут не ? а фрагмент #

https://client.example.com/cb#error=access_denied&state=xyz

описание ошибок - https://datatracker.ietf.org/doc/html/rfc6749#section-4.2.2.1

{
	"error": "invalid_request | unauthorized_client | access_denied | unsupported_response_type | invalid_scope | server_error | temporarily_unavailable",
	"state"?: "",
	"error_description"?: "",
	"error_uri"?: "",
}
```

## 4.3. Resource Owner Password Credentials Grant

- https://datatracker.ietf.org/doc/html/rfc6749#section-4.3
- не рекомендуется использовать

### 4.3.2. Access Token Request

```sh

METHOD: POST
Authorization: Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW
Content-Type: application/x-www-form-urlencoded

https://server.example.com/grant_type=password&username=johndoe&password=A3ddj3w

{
	"grant_type": "password", // password это тип гранта - Resource Owner Password Credentials
	"username": "johndoe", //
	"password": "A3ddj3w", //
	"scope"?: "",  //
}
```

### 4.3.3. Access Token Response

```sh
Status: 200 OK // статус
Content-Type: application/json;charset=UTF-8 // ответ JSON
Cache-Control: no-store // без кеширования
Pragma: no-cache

{
  "access_token":"2YotnFZFEjr1zCsicMWpAA", // токен доступа чаще всего это JWT
  "token_type":"example", // тип токена чаще всего это Bearer
  "expires_in"?:3600, // время жизни токена 1 час
  "refresh_token"?:"tGzv3JOkF0XG5Qx2TlKWIA",  // токен обновления чаще всего это JWT
  "example_parameter"?:"example_value" // доп параметры которые сервер присылает в ответе
}

про refresh токен читать в пункте https://datatracker.ietf.org/doc/html/rfc6749#section-5.1

```

### (5.2) Access Token Error Response

## 4.4. Client Credentials Grant

- https://datatracker.ietf.org/doc/html/rfc6749#section-4.4
- походу без refresh_token

### 4.4.2. Access Token Request

### 4.1.3. Access Token Request

```sh

METHOD: POST
Authorization: Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW
Content-Type: application/x-www-form-urlencoded

https://server.example.com/grant_type=client_credentials

{
	"grant_type": "client_credentials", // client_credentials это тип авторизации - Client Credentials
	"scope"?: "",  //
}
```

### 4.4.3. Access Token Response

```sh
Status: 200 OK // статус
Content-Type: application/json;charset=UTF-8 // ответ JSON
Cache-Control: no-store // без кеширования
Pragma: no-cache // появилась

{
  "access_token":"2YotnFZFEjr1zCsicMWpAA", // токен доступа чаще всего это JWT
  "token_type":"example", // тип токена чаще всего это Bearer
  "expires_in"?:3600, // время жизни токена 1 час
  "example_parameter"?:"example_value" // доп параметры которые сервер присылает в ответе
}

про refresh токен читать в пункте https://datatracker.ietf.org/doc/html/rfc6749#section-5.1

```

### (5.2) Access Token Error Response

## раздел 5 Информация дополняющая гранты выше

- https://datatracker.ietf.org/doc/html/rfc6749#section-5.2

### 5.1. Successful Response (это по суть интерфейс который обрабатывает все успешные ответы)

```sh
Status: 200 OK
Content-Type: application/json;charset=UTF-8
Cache-Control: no-store
Pragma: no-cache

{
  "access_token": "2YotnFZFEjr1zCsicMWpAA",
  "token_type": "example",
  "expires_in"?: 3600,
  "refresh_token"?: "tGzv3JOkF0XG5Qx2TlKWIA",
	"scope"?: "",
  "example_parameter"?:"example_value",
}


```

### 5.2. Error Response (это по суть интерфейс который обрабатывает все неудачные ответы)

```sh
Status: 400 Bad Request
Content-Type: application/json;charset=UTF-8
Cache-Control: no-store
Pragma: no-cache

{
	"error":"invalid_request | invalid_client | invalid_grant | unauthorized_client | unsupported_grant_type | invalid_scope",
	"error_description"?: "", // текст описывающий ошибку
	"error_uri"?: "", //
}

```

## 6. Refreshing an Access Token

- refresh_token необязателен
- При обмене токена обновления на пару access/refresh должен учитываться используемый тип авторизации
  (то есть мы получили токен через implicit grant то мы можем лишь через него и получить ещё раз токен)
- маркер обновления привязывается к клиенту, которому он был выдан

### Refresh token Request

```sh

METHOD: POST
Authorization: Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW
Content-Type: application/x-www-form-urlencoded

https://server.example.com/grant_type=refresh_token&refresh_token=tGzv3JOkF0XG5Qx2TlKWIA


{
	"grant_type": "refresh_token",
	"refresh_token": "tGzv3JOkF0XG5Qx2TlKWIA",
	"scope"?: "...",
}

```

### Refresh token Response Обарабатывать по интерфейсам указанным в пункте 5.1 и 5.2

## 7. Получение доступа к серверу ресурсов - аутентификация на сервере

- типы токена примеры: Bearer (RFC6750), Basic,
- Сервер аутентификации не должен обрабатывать неизвестные ему типы токенов
- Общий реестр ошибок при неудачной аутентификации по токену - https://datatracker.ietf.org/doc/html/rfc6749#section-11.4

## 8. RFC 6750 (Bearer token)

### Authorization Request Header Field

```sh
Authorization: Bearer mF_9.B5f-4.1JqM
server.example.com
```

### URI Query Parameter

```sh
Content-Type: application/x-www-form-urlencoded
server.example.com/resource?access_token=mF_9.B5f-4.1JqM
```

## 9. RFC 7591 (OAuth 2.0 Dynamic Client Registration Protocol)

### Client metadata

```sh
"application/json"

"redirect_uris": ["client-example.ru"], // список всех точек редиректа которые сервер должен разрешать - иные точки редиректа запрещены типо CORS
"token_endpoint_auth_method": "none | client_secret_post | client_secret_basic", // типо каким обзаром клиент авторизируется
"grant_types": ["implicit", "password", "client_credentials", "refresh_token"], // Массив типов грантов которые клиент может использовтаь при авторизации
"response_types"?: ["code", "token"], // Массив типов ответов которые клиент может использовать
"client_name"?: "Мой клиент",
"client_uri"?: "https://client.example.com",
"logo_uri"?: "https://client.example.com/logo.png",
"scope"?: "profile email ...", // у сервера авторизации есть значение по умолчанию
"contacts"?: [""], // список контактов клиента - сервер авторизации может оглашать эти данные
"tos_uri"?: "https://client.example.com/условия обслуживания.docx", // ссылка на документ условий клиента
"policy_uri"?: "https://client.example.com/policy.docx", // ссылка на документ политики конфиденциальности
"jwks_uri"?: "https://client.example.com/jwks", // хз зачем
"jwks"?: "jwks_key", // хз зачем
"software_id"?: "", //
"software_version"?: "", //
```

```sh
+-----------------------------------------------+-------------------+
| grant_types value includes:                   | response_types    |
|                                               | value includes:   |
+-----------------------------------------------+-------------------+
| authorization_code                            | code              |
| implicit                                      | token             |
| password                                      | (none)            |
| client_credentials                            | (none)            |
| refresh_token                                 | (none)            |
| urn:ietf:params:oauth:grant-type:jwt-bearer   | (none)            |
| urn:ietf:params:oauth:grant-type:saml2-bearer | (none)            |
+-----------------------------------------------+-------------------+
```

```sh
«client_name»:
	"client_name#en": "My Client"
	"client_name#ja-Jpan-JP": "\u30AF\u30E9\u30A4\u30A2\u30F3\u30C8\u540D"

«tos_uri»
«policy_uri»
«logo_uri»
«client_uri»

```

### Example Request

```sh
{
"redirect_uris": [ "https://client.example.org/callback", "https://client.example.org/callback2"],
"client_name": "My Example Client",
"client_name#ja-Jpan-JP":  "\u30AF\u30E9\u30A4\u30A2\u30F3\u30C8\u540D",
"token_endpoint_auth_method": "client_secret_basic",
"logo_uri": "https://client.example.org/logo.png",
"jwks_uri": "https://client.example.org/my_public_keys.jwks",
"example_extension_parameter": "example_value"
}
```

### Client Information Response

```sh
Status: 201 Created
Content-Type: application/json
Cache-Control: no-store
Pragma: no-cache

{
	"client_id": "s6BhdRkqt3", // дают понять какой клиент авторизируется на сервере авторизации
	"client_secret"?: "cf136dc3c1fc93f31185e5885805d", // нужна для аутентификации КЛИЕНТА на сервере авторизации
	"client_id_issued_at"?: 2893256800, // когда был выдан client_id
	"client_secret_expires_at": 2893276800, // когда истечет client_secret,
	"redirect_uris": [ "https://client.example.org/callback",  "https://client.example.org/callback2"],
  "grant_types": ["authorization_code", "refresh_token"],
  "client_name": "My Example Client",
  "client_name#ja-Jpan-JP": "\u30AF\u30E9\u30A4\u30A2\u30F3\u30C8\u540D",
  "token_endpoint_auth_method": "client_secret_basic",
  "logo_uri": "https://client.example.org/logo.png",
  "jwks_uri": "https://client.example.org/my_public_keys.jwks",
  "example_extension_parameter": "example_value"
}


```

### Client Registration Error Response

```sh
Status: 400 Bad Request
Content-Type: application/json
Cache-Control: no-store
Pragma: no-cache

{
	"error": "invalid_redirect_uri | invalid_client_metadata | invalid_software_statement | unapproved_software_statement"
	"error_description"?: "The grant type 'authorization_code' must be registered along with the response type 'code' but found only'implicit' instead."
}

```

## 10. OAuth 2.0 Token Introspection

### Introspection Request

```sh

server.example.com/introspect

Method: POST
Accept: application/json
Content-Type: application/x-www-form-urlencoded
Authorization: Bearer 23410913-abewfq.123483

x-www-form-urlencoded

token=2YotnFZFEjr1zCsicMWpAA&token_type_hint=access_token
```

### Introspection Response

```sh

Status:200 OK
Content-Type: application/json

{
 "active": true,
 "client_id"?: "l238j323ds-23ij4",
 "username"?: "jdoe",
 "scope"?: "read write dolphin",
 "sub"?: "Z5O3upPC88QrAjx00dis",
 "aud"?: "https://protected.example.net/resource",
 "iss"?: "https://server.example.com/",
 "exp"?: 1419356238,
 "iat"?: 1419350238,
 "extension_field"?: "twenty-seven"
}

```

## 11. OAuth 2.0 Authorization Server Metadata

### https://datatracker.ietf.org/doc/html/rfc8414#section-2

- https://accounts.google.com/.well-known/openid-configuration

```sh

Point: https://server.example.com/.well-known
Status: 200 OK
Content-Type: application/json

{
	"issuer": "https://server.example.com",
	"authorization_endpoint": "https://server.example.com/authorize",
	"token_endpoint": "https://server.example.com/token",
	"jwks_uri": "https://server.example.com/jwks.json",
	"registration_endpoint": "https://server.example.com/register",
	"scopes_supported": ["openid", "profile", "email", "address", "phone", "offline_access"],
	"response_types_supported": ["code", "code token"],
	"response_modes_supported": [],
	"grant_types_supported": [],
	"token_endpoint_auth_methods_supported": ["client_secret_basic", "private_key_jwt"],
	"token_endpoint_auth_signing_alg_values_supported": ["RS256", "ES256"],
	"service_documentation": "http://server.example.com/service_documentation.html",
	"ui_locales_supported": ["en-US", "en-GB", "en-CA", "fr-FR", "fr-CA"],
	"op_policy_uri": "",
	"op_tos_uri": "",
	"revocation_endpoint": "",
	"revocation_endpoint_auth_methods_supported": [],
	"revocation_endpoint_auth_signing_alg_values_supported": [],
	"introspection_endpoint": "",
	"introspection_endpoint_auth_methods_supported": [],
	"introspection_endpoint_auth_signing_alg_values_supported": [],
	"code_challenge_methods_supported": [],
	# Дополнительно
	"userinfo_endpoint": "https://server.example.com/userinfo",
}
```
