# Помогалки

## Yarn

Обновить пакеты в интерактивном режиме

```sh
yarn plugin import interactive-tools
yarn upgrade-interactive
```

### Установка

- https://yarnpkg.com/getting-started/install

```sh
corepack enable
```

Для node 18 и выше

```sh
corepack prepare yarn@stable --activate
```

Обновление yarn до последней версии

```sh
yarn set version stable
or
yarn set version latest
```

---

### Yarn vscode sdk

Установка плагинов для vscode

```sh
yarn dlx @yarnpkg/sdks vscode
```

Это плагин для автоматического управления @types

```sh
yarn plugin import typescript
```

- https://yarnpkg.com/getting-started/editor-sdks

---

## Шпаргалка по скриптам npm

- https://docs.npmjs.com/cli/v9/using-npm/scripts

---

## Шпаргалка по настройке vscode

- https://code.visualstudio.com/docs/getstarted/settings#_default-settings

---

## editorconfig

Новый стандартизированный конфиг для форматирования кода

- https://editorconfig.org/

---

## Настройка github

- не игнорировать регистр папок в проекте (болезнено для винды)
- завершающий символ lf (в windows он crlf а в ubuntu lf)

Изменить конфиг гита локально

```sh
git config --local core.ignorecase false
git config --local core.autocrlf false
git config --local core.eol lf
```

Изменить конфиг гита глобально

```sh
git config --global core.ignorecase false
git config --global core.autocrlf false
git config --global core.eol lf
```

Изменить конфиг гита вручную

```sh
[core]
	ignorecase = false
	autocrlf = false
	eol = lf
```

---

## Настройка typescript compiler

Рекомендованые настройки под разные штуки

- https://github.com/tsconfig/bases/

Документация по настройке tsconfig

- https://www.typescriptlang.org/tsconfig

Документация по настройке jsconfig

- https://code.visualstudio.com/docs/languages/jsconfig#_jsconfig-options

# Консольные команды

Если уже имеется папка с git то мы можем отправить её в репозиторий

```sh
git remote add origin git@gitlab.com:Pasha1neo/project-tamplate.git
git push -u origin main
```

Сгенерировать новый ключ ssh

```sh
ssh-keygen -t ed25519
```

- Далее он скажет ввести имя файла (ключа)
- Далее он скажет ввести парольную фразу (пароль от ключа)
- Далее он скажет ввести повторно фразу
- Ключ сгенерирован (ПРИВАТНЫЙ И ПУБЛИЧНЫЙ)

Дальше нужно настроить ssh config

Зайди в папку .ssh

~ - это папка пользователя

```sh
cd ~/.ssh
```

и указать конфиг

```sh
Host gitlab.com
PreferredAuthentications publickey
IdentityFile ~/.ssh/[НАЗВАНИЕ ФАЙЛА ЗАКРЫТОГО КЛЮЧА]
```
